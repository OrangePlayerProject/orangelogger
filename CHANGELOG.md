- 0.1 RC
	- Se suben clases iniciales del proyecto.

- 0.2 RC
	- Se añade clase SystemUtil para imprimir mensajes directamente desde el descriptor de archivos stdout.

- 0.3 RC
    - Se cambia es color definido para info.
    - Se optimiza un poco clase Logger.

- 0.4 RC
    - Se añade funcionalidad para imprimir mensajes en color blanco.

- 0.4.1-RC
    - Para windows no se agrega el color, ya que no esta soportado en ansi