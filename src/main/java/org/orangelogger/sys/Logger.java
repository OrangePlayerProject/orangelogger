package org.orangelogger.sys;

import java.io.IOException;

public class Logger {

    public static Logger getLogger(String className, String title, Object msg) {
        return new Logger(className, title, msg);
    }

    public static Logger getLogger(Class clazz, String title, Object msg) {
        return new Logger(clazz, title, msg);
    }

    public static Logger getLogger(String className, Object msg) {
        return new Logger(className, msg);
    }

    public static Logger getLogger(Class clazz, Object msg) {
        return new Logger(clazz, msg);
    }

    public static Logger getLogger(Object classObject, Object msg) {
        return new Logger(classObject.getClass(), msg);
    }

    public static Logger getLogger(Object classObject, String title, Object msg) {
        return new Logger(classObject.getClass(), title, msg);
    }

    public static void showLogger(String className, String title, Object msg) {
        new Logger(className, title, msg).printMsg();
    }

    public static void showLogger(Class clazz, String title, Object msg) {
        new Logger(clazz, title, msg).printMsg();
    }

    public static void showLogger(String className, Object msg) {
        new Logger(className, msg).printMsg();
    }

    public static void showLogger(Class clazz, Object msg) {
        new Logger(clazz, msg).printMsg();
    }

    public static void showLogger(Object classObject, Object msg) {
        new Logger(classObject.getClass().getSimpleName(), msg).printMsg();
    }

    public static void showLogger(Object classObject, String title, Object msg) {
        new Logger(classObject.getClass().getSimpleName(), title, msg).printMsg();
    }

    public static enum TYPE {
        INFO, WARNING, ERROR;
    }

    private String className;
    private String title; // Optional
    private String msg;

    public static final String INFOCOLOR = ConsoleColor.ANSI_GREEN;
    public static final String WARNINGCOLOR = ConsoleColor.ANSI_YELLOW;
    public static final String ERRORCOLOR = ConsoleColor.ANSI_RED;
    public static final String RAWCOLOR = ConsoleColor.ANSI_WHITE;

    public Logger(String className, String title, Object msg) {
        this.className = className;
        this.title = title;
        this.msg = String.valueOf(msg);
    }

    public Logger(Class clazz, String title, Object msg) {
        this(clazz.getSimpleName(), title, msg);
    }

    public Logger(String className, Object msg) {
        this(className, null, msg);
    }

    public Logger(Class clazz, Object msg) {
        this(clazz.getSimpleName(), msg);
    }

    private void appendMsgFormat(StringBuilder sbMsg) {
        sbMsg.append(className).append("-> ");
        if (title != null)
            sbMsg.append(title).append(": ");
        sbMsg.append(msg);
    }

    private String getMsg(String color) {
        StringBuilder sbMsg = new StringBuilder();
        if (SystemInfo.IS_UNIX) {
            sbMsg.append(color);
            appendMsgFormat(sbMsg);
            sbMsg.append(ConsoleColor.RESET);
        }
        else {
            appendMsgFormat(sbMsg);
        }
        return sbMsg.toString();
    }

    private String getRawMsg(String color) {
        return SystemInfo.IS_UNIX ?
                color + msg +
                        ConsoleColor.RESET :
                msg;
    }

    private void println(String str) {
        try {
            SystemUtil.getStdout().write(str.concat("\n").getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void print(String str) {
        try {
            SystemUtil.getStdout().write(str.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void error() {
        println(getMsg(ERRORCOLOR));
    }

    public void info() {
        println(getMsg(INFOCOLOR));
    }

    public void warning() {
        println(getMsg(WARNINGCOLOR));
    }

    public void message() {
        println(getMsg(RAWCOLOR));
    }

    public void rawError() {
        println(getRawMsg(ERRORCOLOR));
    }

    public void rawInfo() {
        println(getRawMsg(INFOCOLOR));
    }

    public void rawWarning() {
        println(getRawMsg(WARNINGCOLOR));
    }

    public void rawMessage() {
        println(getRawMsg(RAWCOLOR));
    }

    public void printMsg() {
        info();
    }

    public void printRawMsg() {
        rawInfo();
    }

    public void printMsg(TYPE type) {
        switch (type) {
            case INFO:
                info();
                break;
            case ERROR:
                error();
                break;
            case WARNING:
                warning();
                break;
        }
    }

    public void printRawMsg(TYPE type) {
        switch (type) {
            case INFO:
                rawInfo();
                break;
            case ERROR:
                rawError();
                break;
            case WARNING:
                rawWarning();
                break;
        }
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMsg() {
        return msg;
    }

    public String getColoredMsg(String color) {
        return getRawMsg(color);
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
